/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloMetodos;


import java.util.Date;

/**
 *
 * @author soare
 */
public class Agenda {
    private int agendaCod;
    private String nomeMed;
    private String nomePac;
    private String turno;
    private Date data;
    private String Status;
    private String procedimento;
    private String PacienteNasc;

    public int getAgendaCod() {
        return agendaCod;
    }

    public void setAgendaCod(int agendaCod) {
        this.agendaCod = agendaCod;
    }

    public String getNomeMed() {
        return nomeMed;
    }

    public void setNomeMed(String nomeMed) {
        this.nomeMed = nomeMed;
    }

    public String getNomePac() {
        return nomePac;
    }

    public void setNomePac(String nomePac) {
        this.nomePac = nomePac;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }



    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getProcedimento() {
        return procedimento;
    }

    public void setProcedimento(String procedimento) {
        this.procedimento = procedimento;
    }

    /**
     * @return the data
     */
    public Date getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(Date data) {
        this.data = data;
    }

    public String getPacienteNasc() {
        return PacienteNasc;
    }

    public void setPacienteNasc(String PacienteNasc) {
        this.PacienteNasc = PacienteNasc;
    }

    /**
     * @return the data
     */
 
    
    
    
}
