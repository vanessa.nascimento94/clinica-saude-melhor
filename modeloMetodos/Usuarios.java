/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloMetodos;

/**
 *
 * @author soare
 */
public class Usuarios {
    private Integer cod_usuario;
    private String nome_usuario;
    private String senha_usuario;
    private String tipo_usuario;
    private String pesquisa_usario;

    /**
     * @return the cod_usuario
     */
    public Integer getCod_usuario() {
        return cod_usuario;
    }

    /**
     * @param cod_usuario the cod_usuario to set
     */
    public void setCod_usuario(Integer cod_usuario) {
        this.cod_usuario = cod_usuario;
    }

    /**
     * @return the nome_usuario
     */
    public String getNome_usuario() {
        return nome_usuario;
    }

    /**
     * @param nome_usuario the nome_usuario to set
     */
    public void setNome_usuario(String nome_usuario) {
        this.nome_usuario = nome_usuario;
    }

    /**
     * @return the senha_usuario
     */
    public String getSenha_usuario() {
        return senha_usuario;
    }

    /**
     * @param senha_usuario the senha_usuario to set
     */
    public void setSenha_usuario(String senha_usuario) {
        this.senha_usuario = senha_usuario;
    }

    /**
     * @return the tipo_usuario
     */
    public String getTipo_usuario() {
        return tipo_usuario;
    }

    /**
     * @param tipo_usuario the tipo_usuario to set
     */
    public void setTipo_usuario(String tipo_usuario) {
        this.tipo_usuario = tipo_usuario;
    }

    /**
     * @return the pesquisa_usario
     */
    public String getPesquisa_usario() {
        return pesquisa_usario;
    }

    /**
     * @param pesquisa_usario the pesquisa_usario to set
     */
    public void setPesquisa_usario(String pesquisa_usario) {
        this.pesquisa_usario = pesquisa_usario;
    }
    
}
