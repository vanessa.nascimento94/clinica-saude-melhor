/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloMetodos;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author soare
 */
public class Tabela extends AbstractTableModel{
    
    private ArrayList linhas =null; //armazena as linhas da tabela
    private String[] colunas =null; //vetor armazena as colunas da tabela
    
    public Tabela(ArrayList lin, String[] col){// vai receber linhas e colunas da tabela
        setLinhas(lin);
        setColunas(col);
        
        
    }

    /**
     * @return the linhas
     */
    public ArrayList getLinhas() {
        return linhas;
    }

    /**
     * @param linhas the linhas to set
     */
    public void setLinhas(ArrayList linhas) {
        this.linhas = linhas;
    }

    /**
     * @return the colunas
     */
    public String[] getColunas() {
        return colunas;
    }

    /**
     * @param colunas the colunas to set
     */
    public void setColunas(String[] colunas) {
        this.colunas = colunas;
    }
    
    public int getColumnCount(){ //metodo conta numero de colunas
        return colunas.length;
        
    }
     public int getRowCount(){ //metodo conta numero de linhas
        return linhas.size();
        
    }
      public String getColumnName(int numCol){ //metodo retorna nome das colunas
        return colunas[numCol];
        
    }
      public Object getValueAt(int numLin, int numCol){
          Object[] linha = (Object[])getLinhas().get(numLin);
          return linha[numCol];
      }

  

    
}
