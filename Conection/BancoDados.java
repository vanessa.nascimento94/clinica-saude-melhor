/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conection;

import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.postgresql.core.SqlCommand;

/**
 *
 * @author soare
 */
public class BancoDados {
    
   public Statement  stm; //pesquisa no banco de dados
   public ResultSet rs; //armazena resultado das pesquisas
   private String driver = "org.postgresql.Driver"; // identifica o banco de dados
   private String caminho = "jdbc:postgresql://localhost:5432/Clinica";
   private String usuario = "postgres"; // usuario do BD
   private String senha = "1234";  //senha do BD
   public Connection con;
    
    public void conexao(){
        
       try {
           System.setProperty("jdbc.Drivers", driver); //setar a propriedade do drive de conexão
           con= DriverManager.getConnection(caminho, usuario, senha);
          // JOptionPane.showMessageDialog(null, "Conectado");
       } catch (SQLException ex) {
           
           JOptionPane.showMessageDialog(null, "Erro ao se conectar \n"+ex.getMessage());
       }
    }
    
    public void executaSql(String sql){
       try {
           stm = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY); //verifica tanto no fim, quanto no começo da tabela
           rs = stm.executeQuery(sql);
       } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null, "Erro ao pesquisar \n"+ex.getMessage());
       }
    
    }
    
   public void desconecta(){
       try {
           con.close();
        //   JOptionPane.showMessageDialog(null, "Desconectado");
       } catch (SQLException ex) {
         JOptionPane.showMessageDialog(null, "Erro ao se desconectar \n"+ex.getMessage());       }
   } 
}
