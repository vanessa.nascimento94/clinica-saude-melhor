/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package telas;

import Conection.BancoDados;
import ModeloControle.ControleAgenda;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import modeloMetodos.Tabela;

/**
 *
 * @author soare
 */
public class FormAgenMedico extends javax.swing.JFrame {

    /**
     * Creates new form FormAgenMedico
     */
    BancoDados conexao = new BancoDados();
    String dtHoje;
    String status;
    ControleAgenda controlagenda = new ControleAgenda();
    String cod_ag;
    
    public FormAgenMedico() {
        initComponents();
        Calendar data = Calendar.getInstance();
        Date d = data.getTime(); // recebe data
      
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        dateFormat.format(d); //formata a data
       
        dtHoje= dateFormat.format(d);//data de hoje
        status = "Em atendimento"; // com os agendamentos no status Em atendimento
        
        preencherMedicos();
    }
    
      public void preencherMedicos(){
        conexao.conexao();
        conexao.executaSql("Select *from medico order by nome");
        try{
            conexao.rs.first();
            jComboBoxMedicos.removeAllItems();
            do{
                jComboBoxMedicos.addItem(conexao.rs.getString("nome"));

            }while(conexao.rs.next());
        }catch(SQLException ex){
            JOptionPane.showMessageDialog(rootPane, "Erro ao preencher medicos"+ex);
        }
       conexao.desconecta();
    }
      
      public void preencherTabela(String Sql){
        ArrayList dados = new ArrayList();   
        String [] colunas = new String[]{"ID","Nome Paciente","Turno","Data","Status", "Medico","Especialidade"};
        conexao.conexao();
        conexao.executaSql(Sql);
        
        try{
            conexao.rs.first();
            do{
                dados.add(new Object[]{conexao.rs.getInt("cod_agenda"),conexao.rs.getString("pac_nome"),conexao.rs.getString("agenda_turno"),conexao.rs.getString("agenda_data"),conexao.rs.getString("agenda_status"),conexao.rs.getString("nome"),conexao.rs.getString("especialidade")});
                
                
            }while(conexao.rs.next()); //enquanto houver dados irá percorrer os dados
        
    }catch(SQLException ex){
            JOptionPane.showMessageDialog(rootPane, "Não há agenda para o médico selecionado");
           
            }
        Tabela modelo = new Tabela(dados, colunas);
        jTablePacAtender.setModel(modelo);
        //coluna ID
        jTablePacAtender.getColumnModel().getColumn(0).setPreferredWidth(40);//  tamanho da coluna
        jTablePacAtender.getColumnModel().getColumn(0).setResizable(false); // não pode alterar tamanho da tabela
        //coluna nome paciente
        jTablePacAtender.getColumnModel().getColumn(1).setPreferredWidth(200);//  tamanho da coluna
        jTablePacAtender.getColumnModel().getColumn(1).setResizable(false); // não pode alterar tamanho da tabela        
        //coluna turno
        jTablePacAtender.getColumnModel().getColumn(2).setPreferredWidth(100);//  tamanho da coluna
        jTablePacAtender.getColumnModel().getColumn(2).setResizable(false); // não pode alterar tamanho da tabela   
        //coluna data
        jTablePacAtender.getColumnModel().getColumn(3).setPreferredWidth(100);//  tamanho da coluna
        jTablePacAtender.getColumnModel().getColumn(3).setResizable(false); // não pode alterar tamanho da tabela 
        //coluna status
        jTablePacAtender.getColumnModel().getColumn(4).setPreferredWidth(110);//  tamanho da coluna
        jTablePacAtender.getColumnModel().getColumn(4).setResizable(false); // não pode alterar tamanho da tabela  
        //coluna nome medico
        jTablePacAtender.getColumnModel().getColumn(5).setPreferredWidth(160);//  tamanho da coluna
        jTablePacAtender.getColumnModel().getColumn(5).setResizable(false); // não pode alterar tamanho da tabela   
        //coluna especialidade medico
        jTablePacAtender.getColumnModel().getColumn(6).setPreferredWidth(120);//  tamanho da coluna
        jTablePacAtender.getColumnModel().getColumn(6).setResizable(false); // não pode alterar tamanho da tabela 
        jTablePacAtender.getTableHeader().setReorderingAllowed(false);// não pode reordenar
        jTablePacAtender.setAutoResizeMode(jTablePacAtender.AUTO_RESIZE_OFF);//não pode redimensionar
        jTablePacAtender.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);//seleciona um item por vez
        conexao.desconecta();
        
    }        
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jComboBoxMedicos = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTablePacAtender = new javax.swing.JTable();
        jButtonAtender = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jButtonBuscar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("AGENDA DO MEDICO");

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jComboBoxMedicos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxMedicosActionPerformed(evt);
            }
        });

        jLabel2.setText("Médico");

        jTablePacAtender.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jTablePacAtender.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTablePacAtenderMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTablePacAtender);

        jButtonAtender.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButtonAtender.setText("ATENDER PACIENTE");
        jButtonAtender.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAtenderActionPerformed(evt);
            }
        });

        jLabel3.setText("Pacientes Em Espera:");

        jButtonBuscar.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jButtonBuscar.setText("Buscar");
        jButtonBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonBuscarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 845, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addContainerGap(24, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jButtonAtender, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(326, 326, 326))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jComboBoxMedicos, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(57, 57, 57))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBoxMedicos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                        .addComponent(jLabel3)
                        .addGap(39, 39, 39))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jButtonBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 336, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButtonAtender, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(374, 374, 374)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(17, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        setSize(new java.awt.Dimension(978, 630));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAtenderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAtenderActionPerformed
        FormConsulta tela = new FormConsulta(cod_ag);
        tela.setVisible(true);
        dispose();
    }//GEN-LAST:event_jButtonAtenderActionPerformed

    private void jComboBoxMedicosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxMedicosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxMedicosActionPerformed

    private void jButtonBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonBuscarActionPerformed
        int cod= controlagenda.buscarCodMedico((String) jComboBoxMedicos.getSelectedItem());
        String codigo = String.valueOf(cod);
        
               preencherTabela("SELECT *from agenda inner join pacientes on agenda_codpac=cod_paciente inner join medico on agenda_codmedico=cod_medico where agenda_codmedico='"+codigo+"' and agenda_data='"+dtHoje+"' and agenda_status='"+status+"' ");

    }//GEN-LAST:event_jButtonBuscarActionPerformed

    private void jTablePacAtenderMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTablePacAtenderMouseClicked
        cod_ag = ""+jTablePacAtender.getValueAt(jTablePacAtender.getSelectedRow(),0);
    }//GEN-LAST:event_jTablePacAtenderMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormAgenMedico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormAgenMedico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormAgenMedico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormAgenMedico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FormAgenMedico().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAtender;
    private javax.swing.JButton jButtonBuscar;
    private javax.swing.JComboBox<String> jComboBoxMedicos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTablePacAtender;
    // End of variables declaration//GEN-END:variables
}
