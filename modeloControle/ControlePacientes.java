/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloControle;

import Conection.BancoDados;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import modeloMetodos.Pacientes;

/**
 *
 * @author soare
 */
public class ControlePacientes {
   Pacientes pac= new Pacientes();
   BancoDados conexao = new BancoDados();
 
 public void Salvar(Pacientes pac){
    
        conexao.conexao();
        try {
            PreparedStatement pst = conexao.con.prepareStatement("insert into pacientes(pac_nome,pac_rg,pac_cpf,pac_telefone,pac_endereco,pac_cep,pac_comple,pac_bairro,pac_nasc) values (?,?,?,?,?,?,?,?,?)"); // salva os valores nas colunas da tabela pacientes
            pst.setString(1, pac.getNomePaciente());
            pst.setString(2, pac.getRg());
            pst.setString(3, pac.getCpf());
            pst.setString(4, pac.getTelefone());
            pst.setString(5, pac.getEndereco());
            pst.setString(6, pac.getCep());
            pst.setString(7,pac.getComplemento());
            pst.setString(8,pac.getBairro());
            pst.setString(9, pac.getNasc());
          
            pst.execute();
            JOptionPane.showMessageDialog(null, "Dados Salvos com Sucesso");
        } catch (SQLException ex) {
             JOptionPane.showMessageDialog(null, "Erro ao Salvar dados /n" +ex);
          
        }
        
        conexao.desconecta();
    }

  public void Editar(Pacientes pac){ //Metodo editar pacientes
        conexao.conexao();
        try {
            PreparedStatement pst = conexao.con.prepareStatement("update pacientes set pac_nome=?, pac_rg=?, pac_cpf=?,pac_telefone=?,pac_endereco=?,pac_cep=?,pac_comple=?, pac_bairro=?, pac_nasc=? where cod_paciente=?"); //alteração de dados na tabela de pacientes no codigo de paciente que será alterado
            pst.setString(1, pac.getNomePaciente());
            pst.setString(2, pac.getRg());
            pst.setString(3, pac.getCpf());
            pst.setString(4, pac.getTelefone());
            pst.setString(5, pac.getEndereco());
            pst.setString(6, pac.getCep());
            pst.setString(7,pac.getComplemento());
            pst.setString(8,pac.getBairro());
            pst.setString(9, pac.getNasc());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Dados Alterados com Sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Alterar Dados /n" +ex);
        }
       
        
        conexao.desconecta();
}
  public Pacientes buscaPaciente(Pacientes pac){ //metodo de pesquisa paciente
        conexao.conexao();
        conexao.executaSql("SELECT *from pacientes WHERE pac_nome like '%" +pac.getPesquisar()+ "%'");// pesquisa pelo nome do paciente
        try {
            
            if(conexao.rs.next()){ // retorna o primeiro resultado que encontra no banco
            pac.setCodPac(conexao.rs.getInt("cod_paciente"));
            pac.setNomePaciente(conexao.rs.getString("pac_nome"));
            pac.setRg(conexao.rs.getString("pac_rg"));
            pac.setCpf(conexao.rs.getString("pac_cpf"));
            pac.setTelefone(conexao.rs.getString("pac_telefone"));
            pac.setEndereco(conexao.rs.getString("pac_endereco"));
            pac.setCep(conexao.rs.getString("pac_cep"));
            pac.setComplemento(conexao.rs.getString("pac_comple"));
            pac.setBairro(conexao.rs.getString("pac_bairro"));
            pac.setNasc(conexao.rs.getString("pac_nasc"));
            
            }

        } catch (SQLException ex) {
            
             JOptionPane.showMessageDialog(null, "Paciente não cadastrado /n"+ex );
            
        }
        
        conexao.desconecta();
        return pac;
    } 
   public void Excluir(Pacientes pac){ //metodo de excluir pacientes
        conexao.conexao();
        try {
            PreparedStatement pst= conexao.con.prepareStatement("delete from pacientes where cod_paciente=?");
            pst.setInt(1, pac.getCodPac());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Dados Excluidos com Sucesso");
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Excluir Dados /n" +ex);
        }
        
        
        
        conexao.desconecta();
        
    }
  
}
