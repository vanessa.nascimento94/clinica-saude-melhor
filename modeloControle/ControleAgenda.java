/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloControle;

import Conection.BancoDados;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import modeloMetodos.Agenda;
import modeloMetodos.Consulta;

/**
 *
 * @author soare
 */
public class ControleAgenda {
    Agenda agenda = new Agenda();
    BancoDados conexao = new BancoDados();
    BancoDados conexaoPaciente = new BancoDados(); // buscar no BD o codigo do paciente
    BancoDados conexaoMedico = new BancoDados(); // buscar no BD o codigo do medico
    BancoDados conexaoAgenda = new BancoDados(); // buscar no BD o codigo do agenda
     Consulta consulta = new Consulta(); 
  //  int codMed;
    int codPac;
    int codMed;
    int codag;
    

  public void Salvar(Agenda agenda){
      buscarMedico(agenda.getNomeMed()); //recebe nome do medico
      buscarPaciente(agenda.getNomePac());//recebe nome paciente
      conexao.conexao();
        try {
            PreparedStatement pst = conexao.con.prepareStatement("insert into agenda (agenda_codpac,agenda_codmedico,agenda_turno,agenda_data,agenda_motivo, agenda_status)values (?,?,?,?,?,?)");
            
              pst.setInt(1, codPac);
              pst.setInt(2, codMed);
              pst.setString(3, agenda.getTurno());
              pst.setDate(4,new java.sql.Date(agenda.getData().getTime()));
              pst.setString(5, agenda.getProcedimento());
              pst.setString(6, agenda.getStatus());
             pst.execute();
             JOptionPane.showMessageDialog(null,"Agendamento salvo com sucesso");
               } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null,"Erro ao Salvar agendamento"+ ex);
        }

  } 
 
     public void buscarMedico(String nomeMedico){
        conexaoMedico.conexao(); 
        conexaoMedico.executaSql("select *from medico where nome= '"+nomeMedico+"'");
        try {
           
           conexaoMedico.rs.first();
           
           codMed= conexaoMedico.rs.getInt("cod_medico");
             
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Medico não cadastrado"+ex);
        } 
    }
   
    public void buscarPaciente(String nomePaciente){
        conexaoPaciente.conexao(); 
        conexaoPaciente.executaSql("SELECT *from pacientes where pac_nome= '"+nomePaciente+"'");
        try {
           
            conexaoPaciente.rs.first();
           codPac=conexaoPaciente.rs.getInt("cod_paciente");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Paciente não cadastrado"+ ex);
        } 
    }
    
    public void Alterar(Agenda agenda){
        conexao.conexao();
      
        try {
            PreparedStatement pst = conexao.con.prepareStatement("update agenda set agenda_status=? where cod_agenda=?"); //alteração de dados na tabela de agenda no codigo de agenda que será alterado
            pst.setString(1,agenda.getStatus());
            pst.setInt(2, agenda.getAgendaCod());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Em Atendimento");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Atender o agendamento /n" +ex);
        }
       
        
        conexao.desconecta();
        
        
    }
     public int buscarCodMedico(String nomeMedico){
        conexaoMedico.conexao(); 
        conexaoMedico.executaSql("select *from medico where nome= '"+nomeMedico+"'");
        try {
           
           conexaoMedico.rs.first();
           
           codMed= conexaoMedico.rs.getInt("cod_medico");
             
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Medico não cadastrado"+ex);
        } 
        return codMed;
    }
     
     public void buscarAgendamento(Agenda agenda){
        conexaoMedico.conexao(); 
        conexaoMedico.executaSql("select *from agenda where agenda_data= '"+agenda.getData()+"'");
        try {
           
           conexaoMedico.rs.first();
           
           codMed= conexaoMedico.rs.getInt("cod_medico");
             
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Agendamento não encontrado"+ex);
        }
     }

      public Agenda buscarAgendaPorCodigo(int cod){
        Agenda agend = new Agenda();
       
        conexao.conexao(); 
        conexao.executaSql("select *from agenda inner join pacientes on agenda_codpac=cod_paciente inner join  medico on agenda_codmedico=cod_medico where cod_agenda='"+cod+"'");
        try {
           
           conexao.rs.first();
           agend.setNomePac(conexao.rs.getString("pac_nome"));
           agend.setNomeMed(conexao.rs.getString("nome"));
           agend.setProcedimento(conexao.rs.getString("agenda_motivo"));
           agend.setPacienteNasc(conexao.rs.getString("pac_nasc"));
           
          // codMed= conexaoMedico.rs.getInt("cod_medico");
             
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao buscar Agendamento"+ex);
        }
        return agend;
     }   
    
     public void SalvarConsulta(Consulta consulta){
     
      conexao.conexao();
        try {
            PreparedStatement pst = conexao.con.prepareStatement("insert into consulta (consult_diagnostico,consult_receita,consult_exame, consult_obs)values (?,?,?,?)");
            
             // pst.setInt(1, codag);
              pst.setString(1, consulta.getDiagnostico());
              pst.setString(2, consulta.getReceita());
              pst.setString(3, consulta.getExame());
              pst.setString(4, consulta.getObs());
          
             pst.execute();
             JOptionPane.showMessageDialog(null,"Agendamento salvo com sucesso");
               } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null,"Erro ao Salvar agendamento"+ ex);
        }
      
     // conexao.desconecta();
    
  } 
     
      public void buscarAgend(int codag){
        conexaoAgenda.conexao(); 
        conexaoAgenda.executaSql("SELECT *from agenda where cod_agenda= '"+codag+"'");
        try {
           
            conexaoAgenda.rs.first();
           codag=conexaoAgenda.rs.getInt("cod_agenda");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"codigo não cadastrado"+ ex);
        } 
    }
    
     public void Atualizar(Consulta consulta){
        conexao.conexao();
      
        try {
            PreparedStatement pst = conexao.con.prepareStatement("update agenda set agenda_status=? where cod_agenda=?"); //alteração de dados na tabela de agenda no codigo de agenda que será alterado
            pst.setString(1,agenda.getStatus());
            pst.setInt(2, agenda.getAgendaCod());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Finalizado");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Atender o agendamento /n" +ex);
        }
       
        
        conexao.desconecta();
        
        
    }
}
