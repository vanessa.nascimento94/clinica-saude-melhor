/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloControle;

import Conection.BancoDados;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modeloMetodos.Medicos;


public class ControleMedico {
    //funcionalidades do cadastro medico
    
    BancoDados conexao = new BancoDados();
    
    Medicos med = new Medicos(); // instanciando a classe de atributos
    
    public void Salvar(Medicos med){
    
        conexao.conexao();
        try {
            PreparedStatement pst = conexao.con.prepareStatement("insert into medico(nome,especialidade,crm,endereco,bairro,uf,sexo) values (?,?,?,?,?,?,?)"); // salva os valores nas colunas da tabela medico
            pst.setString(1, med.getNome());
            pst.setString(2, med.getEspecialidade());
            pst.setInt(3, med.getCrm());
            pst.setString(4, med.getEndereco());
            pst.setString(5, med.getBairro());
            pst.setString(6, med.getUf());
            pst.setString(7, med.getSexo());
          
            pst.execute();
            JOptionPane.showMessageDialog(null, "Dados Salvos com Sucesso");
        } catch (SQLException ex) {
             JOptionPane.showMessageDialog(null, "Erro ao Salvar dados /n" +ex);
          
        }
        
        conexao.desconecta();
    }
    
    public void Editar(Medicos med){ //Metodo editar medicos
        conexao.conexao();
        try {
            PreparedStatement pst = conexao.con.prepareStatement("update medico set nome=?, especialidade=?, crm=?, endereco=?, bairro=?, uf=?, sexo=? where cod_medico=?"); //alteração de dados na tabela de medicos no codigo de medico que será alterado
            pst.setString(1, med.getNome());
            pst.setString(2, med.getEspecialidade());
            pst.setInt(3, med.getCrm());
            pst.setString(4, med.getEndereco());
            pst.setString(5, med.getBairro());
            pst.setString(6, med.getUf());
            pst.setString(7, med.getSexo());
            pst.setInt(8, med.getCodigo());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Dados Alterados com Sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Alterar Dados /n" +ex);
        }
        
        
        
        conexao.desconecta();
}
    
    public void Excluir(Medicos med){ //metodo de excluir medicos
        conexao.conexao();
        try {
            PreparedStatement pst= conexao.con.prepareStatement("delete from medico where cod_medico=?");
            pst.setInt(1, med.getCodigo());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Dados Excluidos com Sucesso");
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Excluir Dados /n" +ex);
        }
        
        
        
        conexao.desconecta();
        
    }
    
    public Medicos buscaMedico(Medicos med){ //metodo de pesquisa
        conexao.conexao();
        conexao.executaSql("SELECT *from medico WHERE nome like '%" +med.getPesquisa()+ "%'");// pesquisa pelo nome do medico
        try {
            
            if(conexao.rs.next()){ // retorna o primeiro resultado que encontra no banco
            med.setCodigo(conexao.rs.getInt("cod_medico"));
            med.setNome(conexao.rs.getString("nome"));
            med.setEspecialidade(conexao.rs.getString("especialidade"));
            med.setCrm(conexao.rs.getInt("crm"));
            med.setEndereco(conexao.rs.getString("endereco"));
            med.setBairro(conexao.rs.getString("bairro"));
            med.setUf(conexao.rs.getString("uf"));
            med.setSexo(conexao.rs.getString("sexo"));
            
            }

        } catch (SQLException ex) {
            
             JOptionPane.showMessageDialog(null, "Medico não cadastrado /n"+ex );
            
        }
        
        conexao.desconecta();
        return med;
    }
    
}
