/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloControle;

import Conection.BancoDados;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import modeloMetodos.Usuarios;


public class ControleUsuario {
    BancoDados conexao = new BancoDados();
    
    Usuarios usu = new Usuarios(); // instanciando a classe de atributos dos usuarios
    
    public void Salvar(Usuarios usu){
    
        conexao.conexao();
        try {
            PreparedStatement pst = conexao.con.prepareStatement("insert into usuarios(nome_usuario,senha_usuario,tipo_usuario) values (?,?,?)"); // salva os valores nas colunas da tabela usuario
            pst.setString(1, usu.getNome_usuario());
            pst.setString(2, usu.getSenha_usuario());
            pst.setString(3, usu.getTipo_usuario());
           
          
            pst.execute();
            JOptionPane.showMessageDialog(null, "Usuario Salvo com Sucesso");
        } catch (SQLException ex) {
             JOptionPane.showMessageDialog(null, "Erro ao Salvar Usuario /n" +ex);
          
        }
        
        conexao.desconecta();
    }
        public Usuarios buscaUsuario(Usuarios usu){ //metodo de pesquisa
        conexao.conexao();
        conexao.executaSql("select *from usuarios where nome_usuario like '%" +usu.getPesquisa_usario()+ "%'");// pesquisa pelo nome do usuario
        try {
            
            if(conexao.rs.next()){// retorna o primeiro resultado que encontra no banco
            usu.setCod_usuario(conexao.rs.getInt("cod_usuario"));
            usu.setNome_usuario(conexao.rs.getString("nome_usuario"));
            usu.setSenha_usuario(conexao.rs.getString("senha_usuario"));
            usu.setTipo_usuario(conexao.rs.getString("tipo_usuario"));
            
        }

        } catch (SQLException ex) {
            
             JOptionPane.showMessageDialog(null, "Usuario não cadastrado /n" );
           
        }
        
        conexao.desconecta();
        return usu;
    }
    
     public void Alterar(Usuarios usu){ //Metodo editar usuario
        conexao.conexao();
        try {
            PreparedStatement pst = conexao.con.prepareStatement("update usuarios set nome_usuario=?, senha_usuario=?, tipo_usuario=? where cod_usuario=?"); //alteração de dados na tabela de usuario no codigo de usuario que será alterado
            pst.setString(1, usu.getNome_usuario());
            pst.setString(2, usu.getSenha_usuario());
            pst.setString(3, usu.getTipo_usuario());
            pst.setInt(4, usu.getCod_usuario());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Usuário Alterado com Sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Alterar Usuário /n" +ex);
        }
        
        
        
        conexao.desconecta();
}
    public void Excluir(Usuarios usu){ //metodo de excluir usuario
        conexao.conexao();
        try {
            PreparedStatement pst= conexao.con.prepareStatement("delete from usuarios where cod_usuario=?");
            pst.setInt(1, usu.getCod_usuario());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Usuário Excluido com Sucesso");
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Excluir Usuário /n" +ex);
        }
        
        
        
        conexao.desconecta();
        
    }
}